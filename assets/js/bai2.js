$().ready(function () {

    $("#province").click(function () {
        let txt = "";
        let url_province = "https://vapi.vnappmob.com/api/province"

        $.ajax({
            url: url_province, success: function (result) {
                for (let province of result['results']) {
                    $("#province").append('<option value=' + province['province_id'] + '>' + province['province_name'] + '</option>')
                }
            }
        });
    });

    $("[id='province']").change(function () {
        let province = $("#province").val();
        if (province != 0) {
            $(this).removeClass("border-danger");


            let txt = "";
            let url_district = "https://vapi.vnappmob.com/api/province/district/" + province

            $.ajax({
                url: url_district, success: function (result) {

                    $("#district").empty();
                    $("#district").append('<option value="0">Quận huyện</option>');
                    $("#district").addClass("border-danger");

                    $("#ward").empty();
                    $("#ward").append('<option value="0">Phường xã</option>');
                    $("#ward").addClass("border-danger");

                    for (let district of result['results']) {

                        if (district['province_id'] == province) {

                            $("#district").append('<option value=' + district['district_id'] + '>' + district['district_name'] + '</option>')
                        }
                    }
                }
            });

        } else {
            $("#district").empty();
            $("#district").append('<option value="0">Quận huyện</option>');
            $("#province").addClass("border-danger");
            $("#district").addClass("border-danger");

            $("#ward").empty();
            $("#ward").append('<option value="0">Phường xã</option>');
            $("#ward").addClass("border-danger");
        }
    });


    $("[id='district']").change(function () {
        let district = $("#district").val();
        if (district != 0) {
            $(this).removeClass("border-danger");

            let url_ward = "https://vapi.vnappmob.com/api/province/ward/" + district

            $.ajax({
                url: url_ward, success: function (result) {
                    console.log(result);

                    $("#ward").empty();
                    $("#ward").append('<option value="0">Phường xã</option>');
                    $("#ward").addClass("border-danger");
                    for (let ward of result['results']) {

                        if (ward['district_id'] == district) {

                            $("#ward").append('<option value=' + ward['ward_id'] + '>' + ward['ward_name'] + '</option>')
                        }
                    }
                }
            });

        } else {
            $("#ward").empty();
            $("#ward").append('<option value="0">Phường xã</option>');
            $("#district").addClass("border-danger");
        }
    });

    $("[id='ward']").change(function () {

        if (this.value != 0) {

            $(this).removeClass("border-danger")
        } else {
            $(this).addClass("border-danger");
        }
    });

});
