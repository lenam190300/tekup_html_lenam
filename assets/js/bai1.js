$().ready(function () {

    $("[id='group-name']").blur(function () {
      
        if (this.value.length > 0) {
            $(this).removeClass("border-danger")
        } else {
            $(this).addClass("border-danger");
        }
    });

    $("[id='description']").blur(function () {
      
        if (this.value.length > 0) {
            $(this).removeClass("border-danger")
        } else {
            $(this).addClass("border-danger");
        }
    });

    $("[id='category']").blur(function () {
      
        if (this.value > 0) {
            $(this).removeClass("border-danger")
        } else {
            $(this).addClass("border-danger");
        }
    });

    $('input[type="file"]').change(function (e) {
        let fileName = e.target.files[0].name;
        $("[name='img']").attr("src", 'assets/images/'+fileName)
    });
});
